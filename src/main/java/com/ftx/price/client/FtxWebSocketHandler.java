package com.ftx.price.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ftx.price.model.FtxRequest;
import com.ftx.price.model.FtxResponse;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

@Component
public class FtxWebSocketHandler extends TextWebSocketHandler {

    private static final Logger LOG = LogManager.getLogger(FtxWebSocketHandler.class);
    private static final String UPDATE_TYPE = "update";

    private ObjectMapper objectMapper;

    @Autowired
    public FtxWebSocketHandler(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws JsonProcessingException {
        LOG.info("Message Received [{}]", message.getPayload());
        FtxResponse ftxResponse = objectMapper.readValue(message.getPayload(), FtxResponse.class);
        if (UPDATE_TYPE.equals(ftxResponse.getType()) && ftxResponse.getData() != null) {
            LOG.info(ftxResponse.getData());
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws IOException {
        LOG.info("New session established : {}", session.getId());

        TextMessage subscribeMessage = new TextMessage(objectMapper.writeValueAsString(getSubscribeMessage()));
        session.sendMessage(subscribeMessage);

        LOG.info("Message sent to websocket server [{}]", subscribeMessage.getPayload());
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
        LOG.error("Transport Error", exception);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        LOG.info("Connection Closed [{}]", status.getReason());
    }

    private FtxRequest getSubscribeMessage() {
        FtxRequest msg = new FtxRequest();
        msg.setOp("subscribe");
        msg.setChannel("ticker");
        msg.setMarket("BTC/USD");
        return msg;
    }
}
