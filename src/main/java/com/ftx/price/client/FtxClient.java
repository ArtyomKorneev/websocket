package com.ftx.price.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.client.WebSocketConnectionManager;
import org.springframework.web.socket.client.standard.StandardWebSocketClient;

@Service
public class FtxClient {

    private static final String URL = "wss://ftx.com/ws/";

    private FtxWebSocketHandler ftxWebSocketHandler;

    @Autowired
    public FtxClient(FtxWebSocketHandler ftxWebSocketHandler) {
        this.ftxWebSocketHandler = ftxWebSocketHandler;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void start() {
        WebSocketConnectionManager connectionManager = new WebSocketConnectionManager(
                new StandardWebSocketClient(), ftxWebSocketHandler, URL);
        connectionManager.start();
    }

}
