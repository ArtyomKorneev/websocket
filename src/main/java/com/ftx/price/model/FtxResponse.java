package com.ftx.price.model;

public class FtxResponse {
    private String type;
    private ResponseData data;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ResponseData getData() {
        return data;
    }

    public void setData(ResponseData data) {
        this.data = data;
    }

    public static class ResponseData {
        private String ask;
        private String bid;

        public String getAsk() {
            return ask;
        }

        public void setAsk(String ask) {
            this.ask = ask;
        }

        public String getBid() {
            return bid;
        }

        public void setBid(String bid) {
            this.bid = bid;
        }

        @Override
        public String toString() {
            return "ask:" + ask +
                    ", bid:" + bid;
        }
    }
}
